select A."PAN",B."NUM_CUENTA",C."NOMBRE",C."ID_SUCURSAL",
E."NOMBRE" "ESTADO" 
from plastico A inner join cuenta B 
on A."ID_CUENTA"=B."NUM_CUENTA"
inner join cliente C on  B."ID_CLIENTE"=C."ID"
inner join sucursal D on  C."ID_SUCURSAL"=D."ID"
inner join estado E on D."ID_ESTADO"=E."ID"
limit 5;
