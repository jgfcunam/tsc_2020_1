from math import sqrt as raiz


class Vector:
    _v = None
    _n = 0

    def __init__(self, x):
        self._v = [float(k) for k in x.split(',')]
        self._n = len(self._v)

    def n_euclid(self):
        return raiz(sum([x ** 2 for x in self._v]))

    def n_supremo(self):
        return max(self._v)

    def normalizar(self):
        ",".join([str(x / self.n_euclid()) for x in self._v])
        return Vector(",".join([str(x / self.n_euclid()) for x in self._v]))

    @property
    def v(self):
        return self._v

    @property
    def n(self):
        return self._n


def principal():
    vectorin = Vector("1,1")
    print(vectorin)
    print(vectorin.v)
    print(vectorin.n)
    print("norma supremo %.4f" % vectorin.n_supremo())
    print("norma euclidea %.6f" % vectorin.n_euclid())
    print(vectorin.normalizar())
    print(vectorin.normalizar().v)
    print(vectorin.normalizar().n_euclid())



if __name__ == '__main__':
    principal()
